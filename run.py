#!/usr/bin/env python3
"""Entry point for the CLI of this project."""
from pop_ml.scripts import start

if __name__ == "__main__":
    start()
