"""Test pop_ml/token/python_keywords.py."""

INPUT = """
The keywords like 'True', 'False', and 'None' should be ignored and remain the same in Python.
""".strip()

EXPECTED = """
Las palabras clave como 'True', 'False', y 'None' deben ser ignorados y permencer iguales en Python.
""".strip()


def test_tokenize(cli_runpy):
    """Test that Python keywords are ignored during translation."""
    ret = cli_runpy(
        INPUT,
        "--translate-to=es",
        "--log-level=debug",
        parse_output=False,
    )
    assert not ret.retcode, ret.stderr
    assert "_KW_" not in ret.stdout


def test_beginning_keyword_tokenized(hub):
    """Verify that python keywords starting a line get tokenized."""
    for keyword in hub.token.python_keywords.BEGINNING_KEYWORDS:
        tokenized = hub.token.python_keywords.tokenize(keyword)
        assert tokenized == hub.token.python_keywords.PLACEHOLDER.format(
            keyword=keyword
        )

        detokenized = hub.token.python_keywords.detokenize(tokenized)
        assert detokenized == keyword


def test_beginning_keyword_not_tokenized(hub):
    """Verify that python keywords starting a line does NOT get tokenized."""
    for keyword in hub.token.python_keywords.BEGINNING_KEYWORDS:
        text = f"other {keyword}"
        tokenized = hub.token.python_keywords.tokenize(text)
        assert tokenized == text

        detokenized = hub.token.python_keywords.detokenize(tokenized)
        assert detokenized == text


def test_keyword_tokenized(hub):
    """Verify that certain python keywords always get tokenized."""
    for keyword in hub.token.python_keywords.ALWAYS_KEYWORDS:
        text = f"other {keyword}"
        tokenized = hub.token.python_keywords.tokenize(text)
        assert tokenized == "other " + hub.token.python_keywords.PLACEHOLDER.format(
            keyword=keyword
        )

        detokenized = hub.token.python_keywords.detokenize(tokenized)
        assert detokenized == text
