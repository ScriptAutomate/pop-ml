"""Fixtures that will be used throughout integration tests."""
from unittest import mock

import pytest


@pytest.fixture(scope="session", name="hub")
def integration_hub(hub):
    """Hub fixture for use within integration tests."""
    hub.pop.sub.add(dyne_name="ml")

    with mock.patch("sys.argv", ["pop-translate"]):
        hub.pop.config.load([], cli="pop_ml", parse_cli=False)

    yield hub
