"""Testing pop_ml/ml/init.py."""
import shutil
import subprocess
import unittest.mock as mock


def test_cli(cli_runpy):
    """Test that we can call the cli tool to translate "Hello World" from english to spanish."""
    ret = cli_runpy(
        "Hello World!", "--translate-to=es", "--log-level=debug", parse_output=False
    )
    assert not ret.retcode, ret.stderr
    assert ret.stdout.strip() == "¡Hola Mundo!"


def test_pop_translate(cli_runpy):
    """Verify that the pop-translate command installed with the package is functional."""
    assert shutil.which("pop-translate"), "pop-ml is not installed to the environment"
    ret = subprocess.check_call(["pop-translate", "--help"])

    assert ret == 0


def test_plugin():
    """Test the plugin usage of pop-ml from the README."""
    import pop.hub

    # Initialize the hub
    hub = pop.hub.Hub()

    # Add the "ml" dynamic namespace to the hub
    hub.pop.sub.add(dyne_name="ml")

    with mock.patch("sys.argv", ["pop-translate", "Hello World!", "--translate-to=es"]):
        # Load config values onto hub.OPT
        hub.pop.config.load(["pop_ml"], cli="pop_ml")

    # Call the idempotent "init" of pop-ml's tokenizer using values from config
    hub.ml.tokenizer.init(
        model_name=hub.OPT.pop_ml.model_name,
        dest_lang=hub.OPT.pop_ml.dest_lang,
        source_lang=hub.OPT.pop_ml.source_lang,
        pretrained_model=hub.OPT.pop_ml.pretrained_model_class,
        pretrained_tokenizer=hub.OPT.pop_ml.pretrained_tokenizer_class,
    )
    # Call the function to translate the text
    result = hub.ml.tokenizer.translate([hub.OPT.pop_ml.input_text])

    assert result == ["¡Hola Mundo!"]
