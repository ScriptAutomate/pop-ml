"""Testing pop_ml/ml/init.py."""


def test_cli(mock_hub, hub):
    """Test that the config load function is properly called."""
    mock_hub.ml.init.cli = hub.ml.init.cli
    mock_hub.ml.init.cli()
    mock_hub.pop.config.load.assert_called_once()
