"""Fixtures that will be used throughout unit tests."""
from unittest import mock

import pytest


@pytest.fixture(scope="session", name="hub")
def unit_hub(hub):
    """Hub fixture for use within unit tests."""
    hub.pop.sub.add(dyne_name="ml")

    with mock.patch("sys.argv", ["pop-translate"]):
        hub.pop.config.load([], cli="pop_ml", parse_cli=False)

    yield hub


@pytest.fixture(scope="function")
def mock_hub(hub):
    """A hub for unit tests that contain only mocked functions.

    Functions from the mock hub are meant for replacing functions in the real hub,
    or vice versa.
    """
    m_hub = hub.pop.testing.mock_hub()
    m_hub.OPT = hub.OPT
    m_hub.SUBPARSER = mock.MagicMock()
    yield m_hub
